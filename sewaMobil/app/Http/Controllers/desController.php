<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class desController extends Controller
{
    public function toyota1()
    {
        return view('des.toyota1');
    }

    public function daihatsu1()
    {
        return view('des.daihatsu1');
    }

    public function honda1()
    {
        return view('des.honda1');
    }

    public function suzuki1()
    {
        return view('des.suzuki1');
    }

    public function wuling1()
    {
        return view('des.wuling1');
    }

    public function mitsu1()
    {
        return view('des.mitsu1');
    }
}
