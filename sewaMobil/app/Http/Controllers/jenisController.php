<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class jenisController extends Controller
{
    public function toyota()
    {
        return view('jenis.toyota');
    }

    public function daihatsu()
    {
        return view('jenis.daihatsu');
    }

    public function honda()
    {
        return view('jenis.honda');
    }

    public function suzuki()
    {
        return view('jenis.suzuki');
    }

    public function wuling()
    {
        return view('jenis.wuling');
    }

    public function mitsu()
    {
        return view('jenis.mitsu');
    }
}
