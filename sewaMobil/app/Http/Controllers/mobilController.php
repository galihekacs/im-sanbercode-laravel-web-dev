<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class mobilController extends Controller
{
    public function home()
    {
        return view('welcome');
    }


    public function mobil()
    {
        return view('menu.mobil');
    }

    public function merek()
    {
        return view('menu.merek');
    }
}
