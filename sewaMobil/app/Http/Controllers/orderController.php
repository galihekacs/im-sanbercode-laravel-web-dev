<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class orderController extends Controller
{
    public function toyota2()
    {
        return view('sewa.toyota2');
    }

    public function daihatsu2()
    {
        return view('sewa.daihatsu2');
    }

    public function honda2()
    {
        return view('sewa.honda2');
    }

    public function suzuki2()
    {
        return view('sewa.suzuki2');
    }

    public function wuling2()
    {
        return view('sewa.wuling2');
    }

    public function mitsu2()
    {
        return view('sewa.mitsu2');
    }
}
