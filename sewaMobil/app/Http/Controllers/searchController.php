<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class searchController extends Controller
{
    public function index()
    {
        return view('search.index');
    }

    public function search(Request $request)
    {
        $keyword = $request->get('keyword');
        $items = Item::where('name', 'like', '%'.$keyword.'%')->get(); // Sesuaikan dengan kolom yang ingin Anda cari

        return view('search.results', compact('items', 'keyword'));
    }
}
