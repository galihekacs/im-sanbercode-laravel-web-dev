<h1>Search Results for "{{ $keyword }}"</h1>

<ul>
    @foreach ($items as $item)
        <li>{{ $item->name }}</li>
    @endforeach
</ul>
