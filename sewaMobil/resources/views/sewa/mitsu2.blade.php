<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
      rel="stylesheet">
  
  
    <title>Sewa mobil coy</title>
  
    <link href="{{asset('/bahan/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  
  
    
    <link rel="stylesheet" href="{{asset('/bahan/assets/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('/bahan/assets/css/templatemo-tale-seo-agency.css')}}">
    <link rel="stylesheet" href="{{asset('/bahan/assets/css/owl.css')}}">
    <link rel="stylesheet" href="{{asset('/bahan/assets/css/animate.css')}}">
    <!--
  
  TemplateMo 582 Tale SEO Agency
  
  https://templatemo.com/tm-582-tale-seo-agency
  
  -->
  </head>
  <body>

    <!-- ***** Preloader Start ***** -->
    <div id="js-preloader" class="js-preloader">
      <div class="preloader-inner">
        <span class="dot"></span>
        <div class="dots">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
    <!-- ***** Preloader End ***** -->
  
    <!-- ***** Pre-Header Area End ***** -->
  
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <nav class="main-nav">
                <!-- ***** Logo Start ***** -->
                <a href="index.html" class="logo">
                  <img src="{{asset('/bahan/assets/images/pinjem.png')}}" alt="" style="max-width: 130px;">
                </a>
                <!-- ***** Logo End ***** -->
                <!-- ***** Menu Start ***** -->
                <ul class="nav">
                  <li><a href="/">Home</a></li>
                  <li><a href="/">mobil</a></li>
                  <li><a href="/merek">merek mobil</a></li>
                  <li><a href="/login">Login</a></li>
                </ul>
                <a class='menu-trigger'>
                  <span>Menu</span>
                </a>
                <!-- ***** Menu End ***** -->
              </nav>
            </div>
          </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->
  
    <div class="page-heading">
        <div class="container">
            <div class="row justify-content-start">
                <div class="col-md-4 mb-4">
                    <div class="card">
                    <img src="{{asset('/bahan/assets/images/expander.jpg')}}" class="img-fluid" alt="">
                  </div>
                </div>
              </div>
        </div>
    </div>

    <h1>Mitsubishi Expander</h1><br><br><br>
    <h3>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nostrum laudantium a accusamus expedita, facilis reiciendis porro dolores dolore officiis consequatur quod praesentium in quaerat! Blanditiis unde impedit mollitia veniam beatae?</h3><br>
    <h2>no kendaraan</h2><br>
    <ul>
      <li>G 4896 nn</li>
    </ul><br><br>
    <h2>Biaya perhari</h2><br>
    <h4>2.500.000/day</h4><br><br>


    <h2> ISI DATA</h2>
    <form action="/action page.html">
        <table>Nama panjang</table>
        <input type="text" name="name"><br><br>
        <table>No nik</table>
        <input type="number" name="number"><br><br>
        <table>tanggal pinjam</table>
        <input type="date" name="date"><br><br>
        <table>tanggal pengembalian</table>
        <input type="date" name="date"><br><br>
        <input type="submit" value="submit"><br>

 <!-- Scripts -->
    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('/bahan/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/bahan/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
  
    <script src="{{asset('/bahan/assets/js/isotope.min.js')}}"></script>
    <script src="{{asset('/bahan/assets/js/owl-carousel.js')}}"></script>
    <script src="{{asset('/bahan/assets/js/tabs.js')}}"></script>
    <script src="{{asset('/bahan/assets/js/popup.js')}}"></script>
    <script src="{{asset('/bahan/assets/js/custom.js')}}"></script>
  
  </body>
  
  </html>