<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
    rel="stylesheet">


  <title>Sewa mobil coy</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('/bahan/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">


  <!-- Additional CSS Files -->
  <link rel="stylesheet" href="{{asset('/bahan/assets/css/fontawesome.css')}}">
  <link rel="stylesheet" href="{{asset('/bahan/assets/css/templatemo-tale-seo-agency.css')}}">
  <link rel="stylesheet" href="{{asset('/bahan/assets/css/owl.css')}}">
  <link rel="stylesheet" href="{{asset('/bahan/assets/css/animate.css')}}">
  <!--

TemplateMo 582 Tale SEO Agency

https://templatemo.com/tm-582-tale-seo-agency

-->
</head>

<body>

  <!-- ***** Preloader Start ***** -->
  <div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
      <span class="dot"></span>
      <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- ***** Preloader End ***** -->

  <!-- ***** Pre-Header Area End ***** -->

  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="main-nav">
            <!-- ***** Logo Start ***** -->
            <a href="index.html" class="logo">
              <img src="{{asset('/bahan/assets/images/pinjem.png')}}" alt="" style="max-width: 100px;">
            </a>
            <!-- ***** Logo End ***** -->
            <!-- ***** Menu Start ***** -->
            <ul class="nav">
              <li><a href="/">Home</a></li>
              <li><a href="/mobil">mobil</a></li>
              <li><a href="/merek">merek mobil</a></li>
              <li><a href="/login">Login</a></li>
            </ul>
            <a class='menu-trigger'>
              <span>Menu</span>
            </a>
            
            
            <!-- ***** Menu End ***** -->
          </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- ***** Header Area End ***** -->

  <div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 align-self-center">
          <div class="caption  header-text">
            <h6>Pinjem Mobil coy</h6>
            <div class="line-dec"></div>
            <h4>Tempat sewa <em>Mobil</em>100% <span>aman modal ktp doang</span></h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates id eos maiores nemo. Ea voluptatem labore ullam, nostrum eius accusantium expedita possimus vitae totam incidunt porro? Iste neque obcaecati eaque!</p>
            <div class="main-button"><a href="#">bikin akun</a></div>
            <span>apa</span>
            <div class="second-button"><a href="#">login</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="video-info section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="video-thumb">
            <img src="{{asset('/bahan/assets/images/qwqw1.png')}}" alt="">
          </div>
        </div>
        <div class="col-lg-6 align-self-center">
          <div class="section-heading">
            <h2>Sewa mobil modal  &amp; KTP</h2>
            <div class="line-dec"></div>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iure repellat, tempore quo dignissimos inventore modi alias delectus nulla dolore dicta accusantium repudiandae ullam enim nisi voluptatum suscipit doloribus, vitae consequatur!</p>
          </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="projects section" id="projects">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="section-heading">
            <h2>berikut daftar <em>MOBIL</em> &amp; <span>yang tersedia</span></h2>
            <div class="line-dec"></div>
            <p>cari sendiri lah</p>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="owl-features owl-carousel">
            <div class="item">
              <img src="{{asset('/bahan/assets/images/mitsubishi.jpg')}}" alt="">
              <div class="down-content">
                <h4>Mitsubishi</h4>
                <a href="/merek-daihatsu"><i class="fa fa-link"></i></a>
              </div>
            </div>
            <div class="item">
              <img src="{{asset('/bahan/assets/images/toyo.png')}}" alt="">
              <div class="down-content">
                <h4>Toyota</h4>
                <a href="/merek-toyota"><i class="fa fa-link"></i></a>
              </div>
            </div>
            <div class="item">
              <img src="{{asset('/bahan/assets/images/honda.jpg')}}" alt="">
              <div class="down-content">
                <h4>Honda</h4>
                <a href="/merek-honda"><i class="fa fa-link"></i></a>
              </div>
            </div>
            <div class="item">
              <img src="{{asset('/bahan/assets/images/daihatsu.jpg')}}" alt="">
              <div class="down-content">
                <h4>Daihatsu</h4>
                <a href="/merek-daihatsu"><i class="fa fa-link"></i></a>
              </div>
            </div>
            <div class="item">
              <img src="{{asset('/bahan/assets/images/wuling.jpg')}}" alt="">
              <div class="down-content">
                <h4>Wuling</h4>
                <a href="/merek-wuling"><i class="fa fa-link"></i></a>
              </div>
            </div>
            <div class="item">
              <img src="{{asset('/bahan/assets/images/suzuki.jpg')}}" alt="">
              <div class="down-content">
                <h4>Suzuki</h4>
                <a href="/merek-suzuki"><i class="fa fa-link"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 



  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('/bahan/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/bahan/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

  <script src="{{asset('/bahan/assets/js/isotope.min.js')}}"></script>
  <script src="{{asset('/bahan/assets/js/owl-carousel.js')}}"></script>
  <script src="{{asset('/bahan/assets/js/tabs.js')}}"></script>
  <script src="{{asset('/bahan/assets/js/popup.js')}}"></script>
  <script src="{{asset('/bahan/assets/js/custom.js')}}"></script>

</body>

</html>