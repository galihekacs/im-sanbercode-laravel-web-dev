<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\mobilController;
use App\Http\Controllers\jenisController;
use App\Http\Controllers\desController;
use App\Http\Controllers\orderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/merek', [mobilController::class, 'merek']);
Route::get('/mobil', [mobilController::class, 'mobil']);

Route::get('/merek-daihatsu', [jenisController::class, 'daihatsu']);
Route::get('/merek-honda', [jenisController::class, 'honda']);
Route::get('/merek-mitsu', [jenisController::class, 'mitsu']);
Route::get('/merek-suzuki', [jenisController::class, 'suzuki']);
Route::get('/merek-toyota', [jenisController::class, 'toyota']);
Route::get('/merek-wuling', [jenisController::class, 'wuling']);

Route::get('/des-daihatsu1', [desController::class, 'daihatsu1']);
Route::get('/des-toyota1', [desController::class, 'toyota1']);
Route::get('/des-mitsu1', [desController::class, 'mitsu1']);
Route::get('/des-wuling1', [desController::class, 'wuling1']);
Route::get('/des-honda1', [desController::class, 'honda1']);
Route::get('/des-suzuki1', [desController::class, 'suzuki1']);

Route::get('/search', 'SearchController@index')->name('search');

Route::get('/infopesanan-daihatsu2', [orderController::class, 'daihatsu2']);
Route::get('/infopesanan-toyota2', [orderController::class, 'toyota2']);
Route::get('/infopesanan-mitsu2', [orderController::class, 'mitsu2']);
Route::get('/infopesanan-wuling2', [orderController::class, 'wuling2']);
Route::get('/infopesanan-honda2', [orderController::class, 'honda2']);
Route::get('/infopesanan-suzuki2', [orderController::class, 'suzuki2']);
