<?php
// require_once 'animal.php';
// require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal ("shaun");

echo "Name :  $sheep->name <br>";
echo "Legs :  $sheep->legs <br>";
echo "Cold blooded :  $sheep->cold_blooded <br> <br>";


$kodok = new Frog ("bukbuk");

echo "Name :  $kodok->name <br>";
echo "Legs :  $kodok->legs <br>";
echo "Cold blooded :  $kodok->cold_blooded <br>";
$kodok->jump2();
echo "<br><br>";


$sunwokong = new Ape ("Kera sakti");

echo "Name :  $sunwokong->name <br>";
echo "Legs :  $sunwokong->legs2 <br>";
echo "Cold blooded :  $sunwokong->cold_blooded <br>";
$sunwokong->Yell();
?>